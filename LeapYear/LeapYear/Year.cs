
using System;

namespace LeapYear
{


	public class Year
	{
		int year;
		
		public Year ()
		{
		
		}	
		
		
		public bool divisibleByFour(int year)
		{
			return (year % 4 == 0);
		}
				
		public bool divisibleByOneHundred(int year)
		{
			return (year % 100 == 0);
		}
		
		public bool divisibleByFourHundred(int year)
		{
			return (year % 400 == 0);
		}
		
		public bool isLeapYear(int year)
		{
			return (divisibleByFour(year) 
			    && !divisibleByOneHundred(year)
			    ||  divisibleByFourHundred(year));
		}
		
		
		public static int Main(string[] args)
		{
		return 0;			
		}
	}
}
