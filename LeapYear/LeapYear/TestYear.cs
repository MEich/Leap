
using System;
using NUnit.Framework;

namespace LeapYear
{


	[TestFixture()]
	public class TestYear
	{

		
		Year year = new Year();
		
		[Test()]
		public void IsYearDivisibleByFour()
		{
			Assert.IsTrue(year.divisibleByFour(16));		
		}
		
		[Test()]
		public void IsYearNotDivisibleByFour()
		{
			Assert.IsFalse(year.divisibleByFour(15));		
		}
		
		[Test()]
		public void IsYearDivisibleByOneHundred()
		{
			Assert.IsTrue(year.divisibleByOneHundred(300));		
		}
		
		[Test()]
		public void IsYearNotDivisibleByOneHundred()
		{
			Assert.IsFalse(year.divisibleByOneHundred(299));		
		}
		
		[Test()]
		public void IsYearDivisibleFourAndByOneHundred()
		{
			Assert.IsTrue(year.divisibleByOneHundred(400));
			Assert.IsTrue(year.divisibleByFour(400));
		}
		
		[Test()]
		public void IsYearDivisibleFourAndNotByOneHundred()
		{
			Assert.IsFalse(year.divisibleByOneHundred(160));
			Assert.IsTrue(year.divisibleByFour(160));
		}
		
		[Test()]
		public void IsYearDivisibleByFourHundred()
		{
			Assert.IsTrue(year.divisibleByFourHundred(1600));
		}
		
		[Test()]
		public void IsLeapYear()
		{
			Assert.IsTrue(year.isLeapYear(2016));
			Assert.IsTrue(year.isLeapYear(400));
			Assert.IsFalse(year.isLeapYear(2015));
			Assert.IsFalse(year.isLeapYear(1997));
		}
		
		
		
	
	}
}
